<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>App Pencatatan Data Mustahik</title>

	<style type="text/css">

	::selection { background-color: #E13300; color: white; }
	::-moz-selection { background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body {
		margin: 0 15px 0 15px;
	}

	p.footer {
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}

	#container {
		margin: 10px;
		border: 1px solid #D0D0D0;
		box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
</head>
<body>

<div id="container">
	<h1>Form Ubah Data Mustahik</h1>

	<div id="body">
		<a href="<?php echo site_url('welcome/index') ?>">Kembali</a>
		<?php 
			if($dataEdit)
			{
				$nik = $dataEdit->nik;
				$nama = $dataEdit->nama;
				$alamat = $dataEdit->alamat;
				$no_telp = $dataEdit->no_telp;
				$jns_kelamin = $dataEdit->jns_kelamin;
				$status = $dataEdit->status;
			}
			else
			{
				$nik = '';
				$nama = '';
				$alamat = '';
				$no_telp = '';
				$jns_kelamin = '';
				$status = '';
			}
		?>
	<form action="<?php echo site_url('welcome/edit/'.$nik) ?>" method = "POST">
		<table >
			<tr>
				<td>NIK</td>
				<td>:</td>
				<td><input type="integer" name="nik" value="<?php echo $nik ?>" readonly="readonly"></td>
			</tr>
			<tr>
				<td>Nama</td>
				<td>:</td>
				<td><input type="text" name="nama" value="<?php echo $nama ?>"></td>
			</tr>
			<tr>
				<td>Alamat</td>
				<td>:</td>
				<td><input type="text" name="alamat" value="<?php echo $alamat ?>"></td>
			</tr>
			<tr>
				<td>Nomor Telepon</td>
				<td>:</td>
				<td><input type="integer" name="no_telp" value="<?php echo $no_telp ?>"></td>
			</tr>
			<tr>
				<td>Jenis Kelamin</td>
				<td>:</td>
				<td>
					<select name="jns_kelamin" value="<?php echo $jns_kelamin ?>">
						<option value="Laki-Laki" <?php if( $jns_kelamin =='Laki-Laki'){echo 'selected';}?>>Laki-Laki</option>
						<option value="Perempuan" <?php if($jns_kelamin =='Perempuan'){echo 'selected';}?>>Perempuan</option>
					</select>
				</td>

			</tr>
			<tr>
				<td>Status</td>
				<td>:</td>
				<td>
					<select name="status" value="<?php echo $status ?>">
						<option value="Belum Menikah" <?php if( $status =='Belum Menikah'){echo 'selected';}?>>Belum Menikah</option>
						<option value="Sudah Menikah" <?php if($status =='Sudah Menikah'){echo 'selected';}?>>Sudah Menikah</option>
					</select>
				</td>
			</tr>
			<tr>
				<td></td>
				<td></td>
				<td><input type="submit" name="simpan" value="simpan"></td>
			</tr>
		</table>
	</form>
	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
</html>